#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  3 14:55:00 2019

@author: gabriel
"""
import pandas as pd
import os

# numero de cotacoes que deseja converter
x = 2500

# Setando diretorio de trabalho 
work_dir = '/home/gabriel/Dropbox/Unipampa/Cursando/PSE/ProjetoPSE'
os.chdir(work_dir)


# transforma csv em formato que leia pelo algoritmo de cluster

# Formato : Nome| variavel1 variavel2

# le arquivo que vai ser convertido 
caminho_do_arquivo = '/home/gabriel/Dropbox/Unipampa/Cursando/PSE/ProjetoPSE/acoesAgrupadas.csv3Dias.csv'

arq = pd.read_csv(caminho_do_arquivo)

#arquivo de salvamento
file = open("cotacoes.txt","w")

#vai converter as primeiras x
file.write(str(x) + '\n')

for i in range(0,x):
    gravar = str(arq.loc[i,'0']) + str(i) + '| ' + str(arq.loc[i,'3']) + " " + str(arq.loc[i,'6']) + " " + str(arq.loc[i,'9']) + " " + str(arq.loc[i,'13']) + "\n"      
    file.write(gravar)
    
file.close()